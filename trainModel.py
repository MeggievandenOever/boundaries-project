from cmath import log
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import joblib
import boto3
from io import BytesIO
import json
### take off 60% off this set (323.629) call this trainTeacher.scv
airline = pd.read_csv('/Users/meggievandenoever/Documents/Deeploy/boundaries/first_data.csv',index_col = 'id')

#remove blank rows on Predicted label if any?
airline.dropna(axis = 0,subset =['Delay'],inplace = True)
y = airline.Delay
X = airline.drop('Delay',axis = 1)
X_train,X_valid,y_train,y_valid = train_test_split(X,y,train_size = 0.8,test_size = 0.2)

#deal with missing values
#total percentage of missing values in data
missing_value = X.isnull().sum()
total_column = np.prod(X.shape)
missing_perc = (missing_value.sum()/total_column)*100
#no missing values

#lets deal with categorical values
categ_col = [col for col in X_train.columns if X[col].dtype =='object']

#check frequency of flight
a = X_train.groupby('Airline').size()
a.sort_values

a = X_train.groupby('Flight').size()
a.loc[a.sort_values(ascending = False) >1]

#grouping categorical values to reduce the dimensonality
airportfrom_cat_values = [col for col in X_train['AirportFrom'].value_counts().sort_values(ascending = False).head(10).index]

#count contribution of top 10 values to dataset
X_train['AirportFrom'].value_counts().sort_values(ascending = False).head(10)

#encoding for top 10 labels on AirportFrom
def one_hot_encoding(data, column, features):
    for label in features:
        data[column+"_"+label] = np.where(data[column] == label,1,0)
    

one_hot_encoding(X_train,'AirportFrom',airportfrom_cat_values)

#lets change the values for all 3 categorical features
for categ in categ_col:
    col_labels = [col for col in X_train[categ].value_counts().sort_values(ascending = False).head(10).index]
    one_hot_encoding(X_train,categ,col_labels)
    one_hot_encoding(X_valid,categ,col_labels)

X_train_final = X_train.drop(['Airline','AirportFrom','AirportTo'],axis = 1)
X_valid_final = X_valid.drop(['Airline','AirportFrom','AirportTo'],axis = 1)

logRegr = LogisticRegression()
logRegr.fit(X_train_final.values,y_train)
print('Score on validation set: '+ str(logRegr.score(X_valid_final.values, y_valid)))

#joblib.dump(logRegr, 'modelTeacher.joblib')

bucket_model = 'mlmodelmeggie'
model_object_key = 'airlines/model.joblib'
s3 = boto3.client('s3')

def object_to_s3(bucket, key, object, object_type):
    buffer = BytesIO()
    joblib.dump(object, buffer)
    buffer.seek(0)
    sent_buffer = s3.put_object(Bucket=bucket, Key=key, Body=buffer)
    return sent_buffer['ResponseMetadata']

object_to_s3(bucket_model, model_object_key, logRegr, 'model')

# Add reference to repo
model_reference = {
    'reference': {
        'blob': {
            'url': "s3://mlmodelmeggie/airlines/model"
            }
        }
    }

with open('reference.json', 'w', encoding='utf-8') as f:
    json.dump(model_reference, f, ensure_ascii=False, indent=4)